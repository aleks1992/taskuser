<?php

namespace App\Http\Controllers;

use App\Tasks;
use App\TaskStatus;
use App\Users;
use App\UsersTask;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class TaskController extends Controller
{
    /**
     * Выгрузка данных Task
     * @param Request $request
     * @return Users[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if (!empty($request->id)) {
            $task = Tasks::where('id', $request->id)->first();
            $task->user = $this->getUser($task->id);
            return $task;
        }
        $list = [];
        if (!empty($request->name)) {
            $list = Tasks::where('name', $request->name)->get();
        }
        $list = Tasks::all();

        foreach ($list as $key => $task) {
            $list[$key]->user = $this->getUser($task->task_id);
        }
        return $list;

    }

    /**
     * Список пользователей Задача
     * @param $tas_id
     * @return array
     */
    private function getUser($tas_id)
    {
        $listId = UsersTask::where('task_id', $tas_id)->get();
        $userList = [];
        foreach ($listId as $value) {
            $userList [] = Users::where('id', $value->user_id)->first();
        }
        return $userList;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Добавления нового task
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $task = new Tasks();
        if (!empty($request->id))
            $user = Tasks::where('id', $request->id)->first();

        $task->name = $request->name;
        $task->descriptions = $request->descriptions;
        $task->status_id = $request->status_id;
        $task->save();

        UsersTask::where('task_id', $task->id)->delete();

        foreach (explode(',', $request->user_id) as $id) {
            if (empty($id)) continue;
            $userTask = new UsersTask();
            $userTask->task_id = $task->id;
            $userTask->user_id = $id;
            $userTask->save();
        }

        return response()->json([
            'message' => 'Задача успешно дабавлена',

        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Users $users
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Получения User по ID
     * @param $id
     * @return mixed
     */
    public function edit(Users $users)
    {
        return Users::where('id', $users->id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Users $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $users)
    {
        $user = Users::where('id', $request->id)->first();
        $user->name = $request->name;
        $user->img = null;
        if (!empty($request->img))
            $user->img = $user->upload($request->img);

        $user->save();

        return response()->json([
            'message' => 'Данные о пользователи успешно обновлены',
        ]);
    }


    public function destroy($id)
    {
        Users::destroy($id);

        return response(null, Response::HTTP_OK);
    }
}
