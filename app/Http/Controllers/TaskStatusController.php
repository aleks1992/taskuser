<?php

namespace App\Http\Controllers;

use App\TaskStatus;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class TaskStatusController extends Controller
{
    /**
     * Выгрузка данных StatusTask
     * @param Request $request
     * @return Users[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        return TaskStatus::all();
    }
}
