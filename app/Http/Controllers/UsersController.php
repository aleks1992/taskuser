<?php

namespace App\Http\Controllers;

use App\Users;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Faker\Generator;

class UsersController extends Controller
{
    /**
     * Выгрузка данных User
     * @param Request $request
     * @return Users[]|\Illuminate\Database\Eloquent\Collection
     */
    public function index(Request $request)
    {
        if (!empty($request->id))
            return Users::where('id', $request->id)->first();
        if (!empty($request->name))
            return Users::where('name', $request->name)->get();
        return Users::all();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Добавления нового User
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new Users();
        $user->img = '';
        if (!empty($request->id))
            $user = Users::where('id', $request->id)->first();
        $user->name = $request->name;
        if (!empty($request->img))
            $user->img = $user->upload($request->img);

        $user->save();

        return response()->json([
            'message' => 'Данные о пользователи успешно сохранены',
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Users $users
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Получения User по ID
     * @param $id
     * @return mixed
     */
    public function edit(Users $users)
    {
        return Users::where('id', $users->id)->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Users $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Users $users)
    {
        $user = Users::where('id', $request->id)->first();
        $user->name = $request->name;
        $user->img = null;
        if (!empty($request->img))
            $user->img = $user->upload($request->img);

        $user->save();

        return response()->json([
            'message' => 'Данные о пользователи успешно обновлены',
        ]);
    }


    public function destroy($id)
    {
        Users::destroy($id);

        return response(null, Response::HTTP_OK);
    }
}
