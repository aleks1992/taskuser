<?php

namespace App;

use App\Traits\Eloquent\Uploadable;
use Illuminate\Database\Eloquent\Model;

class Users extends Model
{

    use Uploadable;
    protected $fillable = ['id','name', 'img'];
}
