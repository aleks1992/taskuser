<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

/*
Route::group([ 'namespace' => 'Api', 'as' => 'api.'], function () {
    Route::resource('user', 'UserController');
});*/
/*
Route::resource('user', 'UsersController')->only([
    'index', 'store',
]);*/

//Route::get('/user', 'UsersController@index')->name('user.index');
//Route::post('/user', 'UsersController@store')->name('user.store');
//Route::delete('/user/{id}', 'UsersController@destroy')->name('user.destroy');
//Route::get('/user/{id}', 'UsersController@show')->name('user.show');

// Еще лучше так:
/*Route::resource('user', 'UsersController')
    ->names([
        'destroy' => 'user.destroy'
    ]);*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::get('/task/status', 'TaskStatusController@index')->name('status.index');

Route::resource('user', 'UsersController')
    ->names([
        'destroy' => 'user.destroy'
    ]);

Route::resource('task', 'TaskController')
    ->names([
        'destroy' => 'task.destroy'
    ]);

/*Route::group([ 'namespace' => 'Api', 'as' => 'api.'], function () {
    Route::resource('task', 'TaskController');
});*/
