import VueRouter from "vue-router";
import User from "./components/user/User";
import UserCreated from "./components/user/UserCreated";
import UserInfo from "./components/user/UserInfo";
import Task from "./components/task/Task";
import TaskCreated from "./components/task/TaskCreated";
import TaskInfo from "./components/task/TaskInfo";

export default new VueRouter({
    routes: [
        {
            name: 'user',
            path: '/user',
            component: User
        },
        {
            name: 'userCreated',
            path: '/user/created',
            component: UserCreated
        },
        {
            name: 'userUpdate',
            path: '/user/update',
            component: UserCreated
        },
        {
            name: 'userInfo',
            path: '/user/info',
            component: UserInfo
        },
        {
            name: 'task',
            path: '/task',
            component: Task

        },
        {
            name: 'taskCreated',
            path: '/task/created',
            component: TaskCreated
        },
        {
            name: 'taskInfo',
            path: '/task/info',
            component: TaskInfo
        }
    ],
    mode: 'history'
})
