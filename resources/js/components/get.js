export const get = {
    methods: {
        get: function (key) {
            let p = window.location.search;
            p = p.match(new RegExp(key + '=([^&=]+)'));
            return p ? p[1] : false;

        }
    }
}
