export const form = {
    methods: {
        submit: function() {
            if (!this.isValid)
                return;
            let config = this.getForm();
            axios.post('/api/' + this.nameController, config.formData, config.config)
                .then(response => {
                    window.location.replace('/' + this.nameController);
                })
                .catch(error => console.log(error))
        },
        findForm: function (id) {
            axios.get('/api/' + this.nameController + '?id=' + id)
                .then(response => {
                    this.loadingForm(response.data);
                })
                .catch(error => console.log(error))
        },
        delete: function (id) {
            axios.delete('/api/' + this.nameController+'${id}')
                .then(response => {
                    window.location.replace('/' + this.nameController);
                })
                .catch(error => console.log(error))
        }
    }
};
